<?php

namespace Drupal\Tests\sharepoint_share_link_filter\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\sharepoint_share_link_filter\Plugin\Filter\SharepointShareLink;

/**
 * Test the sharepoint URl filter.
 *
 * @group sharepoint_share_link_filter
 */
class SharepointSharelinkFilterTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'sharepoint_file_download',
    'sharepoint_share_link_filter',
  ];

  /**
   * Test that things work OK with the download filter.
   */
  public function testSharePointDownloadFilter() {
    $filter = SharepointShareLink::create($this->container, [
      'settings' => [
        'prefix' => 'https://example.com/:b:/s/SubSite',
      ],
    ], 'sharepoint_share_link', [
      'provider' => 'sharepoint_share_link_filter',
    ]);
    $result = $filter->process('<div class="field__item">
<p>some text with no link </p>



<p>test the <a href="https://example.com/:b:/s/SubSite/ETe478m6dFVJreM9yHGk9nYBELYahRK7fyL8DYI9ngtW1A?e=o5Oj6g">link</a> 123</p>
</div>', 'nb');
    // We assume, since this one is actually a match for the prefix, that it
    // will be replaced.
    self::assertStringContainsString('/sharepoint-file-download/', $result->getProcessedText(), 'The link was not rewritten to a download link.');

    $result = $filter->process('<div class="field__item">
<p>some text with no link </p>



<p>test the <a href="https://not-example.com/:b:/s/SubSite/ETe478m6dFVJreM9yHGk9nYBELYahRK7fyL8DYI9ngtW1A?e=o5Oj6g">link</a> 123</p>
</div>', 'nb');
    // This one is not a match for the prefix, so it should not be replaced.
    self::assertStringNotContainsString('/sharepoint-file-download/', $result->getProcessedText(), 'The link was rewritten to a download link.');
  }

}
