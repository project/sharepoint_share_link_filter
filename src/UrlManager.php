<?php

namespace Drupal\sharepoint_share_link_filter;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Url;

/**
 * Service for doing things with the URLs we want to do things with.
 */
class UrlManager {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  private ModuleHandlerInterface $moduleHandler;

  /**
   * Constructor.
   */
  public function __construct(ModuleHandlerInterface $module_handler) {
    $this->moduleHandler = $module_handler;
  }

  /**
   * Replace the URL in question if applicable.
   */
  public function replaceUrlIfApplicable($href, $prefix) {
    if (strpos($href, $prefix) === FALSE) {
      return FALSE;
    }
    // Create a so called "shareIdOrEncodedSharingUrl" from the URL. This
    // is documented here:
    // https://learn.microsoft.com/en-us/graph/api/shares-get?view=graph-rest-1.0&tabs=http#encoding-sharing-urls
    // First base 64 encode it.
    $share_id = base64_encode($href);
    // Convert the base64 encoded result to unpadded base64url format by
    // removing = characters from the end of the value...
    $share_id = rtrim($share_id, '=');
    // ... and replacing / with _ and + with -.
    $share_id = str_replace(['/', '+'], ['_', '-'], $share_id);
    // Append u! to be beginning of the string.
    // NOTE! I did not come up with the above description. I mean. Append
    // to the beginning. Isn't that called prepend? Oh my I am so pedantic
    // for pointing it out. And of course I feel extra pedantic when the
    // author of it says Microsoft.
    $share_id = sprintf('u!%s', $share_id);
    return $this->generateUrl($share_id);
  }

  /**
   * Generates the download URL.
   */
  protected function generateUrl($share_id) {
    $url = Url::fromRoute('sharepoint_file_download.download', [
      'file_id' => $share_id,
    ]);
    $this->moduleHandler->alter('sharepoint_share_link_filter_url', $url, $share_id);
    return $url->setAbsolute()->toString();
  }

}
