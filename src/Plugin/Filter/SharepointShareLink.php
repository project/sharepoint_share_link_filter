<?php

namespace Drupal\sharepoint_share_link_filter\Plugin\Filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Drupal\sharepoint_share_link_filter\UrlManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Provides a 'Sharepoint share link' filter.
 *
 * @Filter(
 *   id = "sharepoint_share_link",
 *   title = @Translation("Sharepoint share link"),
 *   description="Converts Sharepoint share links to download links.",
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 *   settings = {
 *     "prefix" = "",
 *   },
 *   weight = -10
 * )
 */
class SharepointShareLink extends FilterBase implements ContainerFactoryPluginInterface {

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * URL manager.
   *
   * @var \Drupal\sharepoint_share_link_filter\UrlManager
   */
  private UrlManager $urlManager;

  /**
   * Constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, UrlManager $url_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->urlManager = $url_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('sharepoint_share_link_filter.url_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form['prefix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Share prefix'),
      '#default_value' => $this->settings['prefix'] ?? '',
      '#description' => $this->t('Provide a prefix to identify the share links. If your share link looks like this: https://mydomain.sharepoint.com/:b:/s/MySite/AbabEffef?e=54Ot6g, then you probably want to input https://mydomain.sharepoint.com/:b:/s/MySite'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $result = new FilterProcessResult($text);
    try {
      $crawler = new Crawler($text);
      $links = $crawler->filter('a');
      $links->each(function (Crawler $node, $i) use (&$text) {
        $link = $node->getNode(0);
        if (!$link instanceof \DOMElement) {
          return;
        }
        $href = $link->getAttribute('href');
        $new_url = $this->replaceUrlIfApplicable($href);
        if (!$new_url) {
          return;
        }
        $text = str_replace($href, $new_url, $text);
      });
      $result->setProcessedText($text);
    }
    catch (\Throwable $e) {
    }
    return $result;
  }

  /**
   * Convenience thing that actually uses the manager.
   */
  public function replaceUrlIfApplicable($href) {
    return $this->urlManager->replaceUrlIfApplicable($href, $this->settings['prefix']);
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    return $this->t('Sharepoint Share URLs will be converted');
  }

}
